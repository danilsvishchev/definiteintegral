package com.pnzgu.uniswing.service;

import com.pnzgu.uniswing.component.DefiniteIntegral;
import com.pnzgu.uniswing.model.IntegrationUnitTableModel;
import com.pnzgu.uniswing.model.IntegrationUnit;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AsynchIntegral extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(AsynchIntegral.class);
    private final IntegrationUnitTableModel model;
    private final Integer selectedRow;
    private final Integer threadAmount;
    private volatile Double result;

    public AsynchIntegral(IntegrationUnitTableModel model, Integer selectedRow, Integer threadAmount) {
        this.model = model;
        this.selectedRow = selectedRow;
        this.threadAmount = threadAmount;
        this.result = 0.0;
    }

    @Override
    public void run() {
        IntegrationUnit row = model.getRow(selectedRow);
        if ((row.getHighLimit() - row.getLowLimit()) / threadAmount < 1) {
            DefiniteIntegral definiteIntegral = new DefiniteIntegral(row.getLowLimit(), row.getHighLimit(), row.getStep(), row.getRound());
            result = definiteIntegral.process();
        } else {
            Thread[] threads = new Thread[threadAmount];
            int len = (row.getHighLimit() - row.getLowLimit()) / threadAmount;
            for (int i = 0; i < threadAmount; i++) {
                final Integer lowLimit = row.getLowLimit() + i * len;
                final Integer highLimit = i + 1 == threadAmount ? row.getHighLimit() : row.getLowLimit() + (i + 1) * len;
                // надо смотреть в сторону ExecutorService
                threads[i] = new Thread(() -> integralProcessing(row, lowLimit, highLimit), "Thread: " + i);
                threads[i].start();
            }

            for (Thread thread : threads) {
                try {
                    thread.join();
                } catch (InterruptedException ex) {
                    logger.error(ex.getMessage());
                }
            }

        }

        model.setValueAt(BigDecimal.valueOf(result).setScale(row.getRound(), RoundingMode.HALF_UP).toString(), selectedRow, 3);
        JOptionPane.showMessageDialog(null, "Значение успешно рассчитано", "Информация", JOptionPane.INFORMATION_MESSAGE);
    }

    private void integralProcessing(IntegrationUnit row, Integer lowLimit, Integer highLimit) {
        logger.info("Thread is working: {}", Thread.currentThread().getName());
        DefiniteIntegral definiteIntegral = new DefiniteIntegral(lowLimit, highLimit, row.getStep(), row.getRound());
        Double process = definiteIntegral.process();
        addToResult(process);
    }

    public synchronized void addToResult(Double value) {
        this.result += value;
    }
}
