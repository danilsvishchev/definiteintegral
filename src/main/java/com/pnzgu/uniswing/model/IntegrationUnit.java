package com.pnzgu.uniswing.model;

import java.io.Serializable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * Модель строки таблицы интеграции
 *
 * @author danil
 */
public class IntegrationUnit implements Serializable {
    private static final long serialVersionUID = 123456789L;
    
    private int lowLimit;
    private int highLimit;
    @Min(1)
    private int step;
    private String result;
    @Min(1)
    private Integer round;
    
    public IntegrationUnit() {
        // empty for Jackson
    }
    
    public IntegrationUnit(int lowLimit, int highLimit, int step) {
        this.lowLimit = lowLimit;
        this.highLimit = highLimit;
        this.step = step;
    }
    
    public IntegrationUnit(@NotBlank String lowLimit, @NotBlank String highLimit, @NotBlank String step, Integer round) {
        this.lowLimit = Integer.parseInt(lowLimit);
        this.highLimit = Integer.parseInt(highLimit);
        this.step = Integer.parseInt(step);
        this.round = round;
    }

    /**
     * @return the lowLimit
     */
    public int getLowLimit() {
        return lowLimit;
    }

    /**
     * @param lowLimit the lowLimit to set
     */
    public void setLowLimit(int lowLimit) {
        this.lowLimit = lowLimit;
    }

    /**
     * @return the highLimit
     */
    public int getHighLimit() {
        return highLimit;
    }

    /**
     * @param highLimit the highLimit to set
     */
    public void setHighLimit(int highLimit) {
        this.highLimit = highLimit;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }
    
    /**
     * @return the round
     */
    public Integer getRound() {
        return round;
    }

    /**
     * @param round the result to set
     */
    public void setRound(Integer round) {
        this.round = round;
    }
    
    @Override
    public String toString() {
        return "[lowLimit: " + lowLimit +
               ", highLimit: "+ highLimit + 
               ", step: "+ step +
               ", result: "+ result + 
               ", round: "+ round + "]";
    }
    
}
