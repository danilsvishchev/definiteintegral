package com.pnzgu.uniswing.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Представление таблицы расчета определенного интеграла
 *
 * @author danil
 */
public class IntegrationUnitTableModel extends AbstractTableModel {

    private static final Logger logger = LoggerFactory.getLogger(IntegrationUnitTableModel.class);

    private static final String[] columnNames = new String[]{
        "Нижняя граница", "Верхняя граница", "Шаг интегрирования", "Результат"};
    private static final Class[] columnsClass = new Class[]{
        Integer.class, Integer.class, Integer.class, String.class
    };
    private List<IntegrationUnit> data = new ArrayList<>();

    public IntegrationUnitTableModel() {
        // empty
    }

    public IntegrationUnitTableModel(List<IntegrationUnit> units) {
        this.data = units;
    }

    public IntegrationUnit getRow(int rowIndex) {
        try {
            return data.get(rowIndex);
        } catch (IndexOutOfBoundsException e) {
            logger.error("Get row failed: {}", e.getMessage());
            return null;
        }
    }

    public void addRow(IntegrationUnit unit) {
        data.add(unit);
        fireTableDataChanged();
    }

    public void removeRow(IntegrationUnit unit) {
        if (data.contains(unit)) {
            data.remove(unit);
            fireTableDataChanged();
        }
    }

    public void removeRow(int rowIndex) {
        try {
            data.remove(rowIndex);
            fireTableDataChanged();
        } catch (IndexOutOfBoundsException e) {
            logger.error("Remove row failed: {}", e.getMessage());
        }
    }
    
    public void removeAllRows() {
        data.clear();
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnsClass[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // придумать как изменить - через enum будет красиво
        return columnIndex != 3;
    }

    // переделать через Map
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        IntegrationUnit unit = data.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return unit.getLowLimit();
            case 1:
                return unit.getHighLimit();
            case 2:
                return unit.getStep();
            case 3:
                return unit.getResult();
            default:
                throw new IllegalArgumentException("Column with index not found");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                data.get(rowIndex).setLowLimit((Integer) aValue);
                break;
            case 1:
                data.get(rowIndex).setHighLimit((Integer) aValue);
                break;
            case 2:
                data.get(rowIndex).setStep((Integer) aValue);
                break;
            case 3:
                data.get(rowIndex).setResult((String) aValue);
                break;
            default:
                break;
        }
        fireTableDataChanged();
    }

    public List<IntegrationUnit> getData() {
        return data;
    }
    
    public void addAllData(Collection<IntegrationUnit> newData) {
        this.data.addAll(newData);
        fireTableDataChanged();
    }

    public void setData(List<IntegrationUnit> data) {
        this.data = data;
    }

}
