/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnzgu.uniswing.component;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * Валидатор вводимых данных в текстовые поля - данные для нахождения 
 * определенного интеграла (возможно стоит сделать статическим и переместить 
 * в утилитный класс)
 *
 * @author danil
 */
public class IntegrationTextInputVerifier extends InputVerifier {

    @Override
    public boolean verify(JComponent input) {
        String text = ((JTextField) input).getText();
        try {
            Integer value = Integer.parseInt(text);
            input.setBorder(BorderFactory.createMatteBorder(2, 1, 1, 1, Color.gray));
            return true;
        } catch (NumberFormatException e) {
            input.setBorder(BorderFactory.createLineBorder(Color.red));
            TextFieldUtils.cleanTextFields((JTextField)input);
            return false;
        }
    }
    
}
