package com.pnzgu.uniswing.component;

import com.pnzgu.uniswing.exception.TextInputFieldException;
import static java.lang.Integer.parseInt;
import javax.swing.JFrame;
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;

/**
 * Утилитный класс для работы с текстовыми полями
 * 
 * @author danil
 */
public final class TextFieldUtils {
    
    public static void cleanTextFields(JTextField... params) {
        for (JTextField field : params) {
            field.setText("");
        }
    }
    
    public static void checkFields(String lowLimitTextField, String highLimitTextField, String stepLimitTextField) throws TextInputFieldException {
        if (StringUtils.isEmpty(lowLimitTextField) || StringUtils.isEmpty(highLimitTextField) || StringUtils.isEmpty(stepLimitTextField)) {
            throw new TextInputFieldException("Проверьте поля, необходимые для вычисления");
        }
        if (parseInt(highLimitTextField)  < parseInt(lowLimitTextField)) {
            throw new TextInputFieldException("Верхняя граница меньше нижней границы");
        }
        if (( parseInt(highLimitTextField) - parseInt(lowLimitTextField) ) < parseInt(stepLimitTextField)){
            throw new TextInputFieldException("Слишком большой шаг интеграции");
        }
    }
}
