package com.pnzgu.uniswing.exception;

public class SerializationException extends Exception {
    private final String description;

    public SerializationException(String description, String message) {
        super(message, null, true, false);
        this.description = description;
    }    

    public String getDescription() {
        return description;
    }
}
