package com.pnzgu.uniswing.exception;

public class TextInputFieldException extends Exception {
    private final String description;
    
    public TextInputFieldException (String description) {
        super(description, null, true, false);
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
}
