package com.pnzgu.uniswing.component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Модель для расчета определенного интеграла методом трапеции
 */
public class DefiniteIntegral {
    private final Integer lowLimit;
    private final Integer highLimit;
    private final Integer step;
    private final Integer round;

    public DefiniteIntegral(@NotNull Integer lowLimit,
                            @NotNull Integer highLimit,
                            @NotNull @Min(1) Integer step,
                            @NotNull @Min(1) Integer round) {
        this.lowLimit = lowLimit;
        this.highLimit = highLimit;
        this.step = step;
        this.round = round;
    }

    /**
     * Расчет определенного интеграла функции cos(x)
     *
     * @return значение интеграла
     */
    public Double process() {
        int n = (highLimit - lowLimit) / step;
        double square = 0;

        if (n == 0) return 0.0;

        for (double i = lowLimit + step; i < highLimit; i += step) {
            square += Math.cos(i);
        }

        square += (Math.cos(lowLimit) + Math.cos(highLimit)) / 2;
        return BigDecimal.valueOf(square * (highLimit - lowLimit) / n).setScale(round, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * @return the lowLimit
     */
    public Integer getLowLimit() {
        return lowLimit;
    }

    /**
     * @return the highLimit
     */
    public Integer getHighLimit() {
        return highLimit;
    }

    /**
     * @return the step
     */
    public Integer getStep() {
        return step;
    }

    /**
     * @return the round
     */
    public Integer getRound() {
        return round;
    }

}
