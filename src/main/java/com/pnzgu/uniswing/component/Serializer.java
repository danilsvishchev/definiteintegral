package com.pnzgu.uniswing.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.pnzgu.uniswing.exception.SerializationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public final class Serializer {

    public static void binarySerialization(Object obj, String file) throws SerializationException {
        try (final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOutputStream.writeObject(obj);
        } catch (IOException ex) {
            throw new SerializationException("Ошибка при выполнение операции сериализации объекта(ов)", ex.getMessage());
        }
    }

    public static <T> List<T> binaryDeserialization(File file, Class<T> target) throws SerializationException {
        try (final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            return (List<T>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            throw new SerializationException("Ошибка при выполнение операции десериализации объекта(ов)", ex.getMessage());
        }
    }

    public static void textSerialization(Object obj, String file) throws SerializationException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File(file), obj);
        } catch (IOException ex) {
            throw new SerializationException("Ошибка при выполнение операции сериализации объекта(ов)", ex.getMessage());
        }
    }
    
    public static <T> List<T> textDeserialization(File file, Class<T> tClass) throws SerializationException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            CollectionType type = mapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);
            return mapper.readValue(file, type);
        } catch (IOException ex) {
            throw new SerializationException("Ошибка при выполнение операции десериализации объекта(ов)", ex.getMessage());
        }
    }

}
