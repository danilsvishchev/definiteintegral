package com.pnzgu.uniswing;

import com.pnzgu.uniswing.model.IntegrationUnit;
import com.pnzgu.uniswing.service.AsynchIntegral;
import com.pnzgu.uniswing.model.IntegrationUnitTableModel;
import com.pnzgu.uniswing.component.IntegrationTextInputVerifier;
import com.pnzgu.uniswing.component.Serializer;
import com.pnzgu.uniswing.component.TextFieldUtils;
import com.pnzgu.uniswing.exception.SerializationException;
import com.pnzgu.uniswing.exception.TextInputFieldException;
import java.awt.EventQueue;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainUI extends JFrame {
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        integrationTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        lowLimitLabel = new javax.swing.JLabel();
        highLimitLabel = new javax.swing.JLabel();
        stepLimitLabel = new javax.swing.JLabel();
        lowLimitTextField = new javax.swing.JTextField();
        highLimitTextField = new javax.swing.JTextField();
        stepLimitTextField = new javax.swing.JTextField();
        jSpinnerSimbolsAfterComma = new javax.swing.JSpinner();
        jLabelSimbolsAfterComma = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        processButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        SaveToTextButton = new javax.swing.JButton();
        UploadFromTextButton = new javax.swing.JButton();
        SaveToBinaryButton = new javax.swing.JButton();
        UploadFromBinaryButton = new javax.swing.JButton();
        deleateAllRowsjButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        integrationTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Нижняя граница", "Верхняя граница", "Шаг интеграции", "Результат"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(integrationTable);
        if (integrationTable.getColumnModel().getColumnCount() > 0) {
            integrationTable.getColumnModel().getColumn(0).setResizable(false);
            integrationTable.getColumnModel().getColumn(1).setResizable(false);
            integrationTable.getColumnModel().getColumn(2).setResizable(false);
            integrationTable.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Данные"));

        lowLimitLabel.setText("Нижняя граница");

        highLimitLabel.setText("Верхняя граница");

        stepLimitLabel.setText("Шаг интеграции");

        jLabelSimbolsAfterComma.setText("Символов после запятой");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(highLimitLabel)
                    .addComponent(lowLimitLabel)
                    .addComponent(jLabelSimbolsAfterComma)
                    .addComponent(stepLimitLabel))
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jSpinnerSimbolsAfterComma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lowLimitTextField)
                    .addComponent(stepLimitTextField)
                    .addComponent(highLimitTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lowLimitLabel)
                    .addComponent(lowLimitTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(highLimitTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(highLimitLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stepLimitLabel)
                    .addComponent(stepLimitTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jSpinnerSimbolsAfterComma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelSimbolsAfterComma))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Действия"));

        processButton.setText("Рассчитать");
        processButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processButtonActionPerformed(evt);
            }
        });

        addButton.setText("Добавить");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        deleteButton.setText("Удалить");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        SaveToTextButton.setText("Сохранить в тексовый файл");
        SaveToTextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveToTextButtonActionPerformed(evt);
            }
        });

        UploadFromTextButton.setText("Загрузка из текстового файла");
        UploadFromTextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UploadFromTextButtonActionPerformed(evt);
            }
        });

        SaveToBinaryButton.setText("Сохранить в бинарный файл");
        SaveToBinaryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveToBinaryButtonActionPerformed(evt);
            }
        });

        UploadFromBinaryButton.setText("Загрузить из бинарного файла");
        UploadFromBinaryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UploadFromBinaryButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(UploadFromTextButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(processButton, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                    .addComponent(addButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(SaveToTextButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(SaveToBinaryButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(UploadFromBinaryButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(processButton)
                .addGap(18, 18, 18)
                .addComponent(addButton)
                .addGap(18, 18, 18)
                .addComponent(deleteButton)
                .addGap(18, 18, 18)
                .addComponent(SaveToTextButton)
                .addGap(18, 18, 18)
                .addComponent(UploadFromTextButton)
                .addGap(18, 18, 18)
                .addComponent(SaveToBinaryButton)
                .addGap(18, 18, 18)
                .addComponent(UploadFromBinaryButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        deleateAllRowsjButton.setText("Очистить таблицу");
        deleateAllRowsjButton.setMaximumSize(new java.awt.Dimension(92, 30));
        deleateAllRowsjButton.setMinimumSize(new java.awt.Dimension(92, 30));
        deleateAllRowsjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleateAllRowsButtonActionPerfomed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deleateAllRowsjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(18, 18, 18)
                        .addComponent(deleateAllRowsjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        try {
            TextFieldUtils.checkFields(lowLimitTextField.getText(), highLimitTextField.getText(), stepLimitTextField.getText());
            
            IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();        
            model.addRow(new IntegrationUnit(lowLimitTextField.getText(), highLimitTextField.getText(), stepLimitTextField.getText(), (Integer) jSpinnerSimbolsAfterComma.getValue()));
            TextFieldUtils.cleanTextFields(lowLimitTextField, highLimitTextField, stepLimitTextField);        
            JOptionPane.showMessageDialog(this, "Данные успешно добавлены", INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
            
        } catch (TextInputFieldException ex) {
            JOptionPane.showMessageDialog(this, ex.getDescription(), ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_addButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        int selectedRow = integrationTable.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this, "Пожалуйста, выберите строку для удаления", ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();
        model.removeRow(selectedRow);
        
        JOptionPane.showMessageDialog(this, "Данные успешно удалены", INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void processButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processButtonActionPerformed
        int selectedRow = integrationTable.getSelectedRow();
        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this, "Пожалуйста, выберите строку для расчета", ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();
        // TODO:не нравится что на прямую указывается индекс - переделать колонки через enum
        Thread thread = new AsynchIntegral(model, selectedRow, 3);
        thread.start();
    }//GEN-LAST:event_processButtonActionPerformed

    private void deleateAllRowsButtonActionPerfomed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleateAllRowsButtonActionPerfomed
        IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();
        if (model.getRowCount() < 1) {
            JOptionPane.showMessageDialog(this, "Таблица пустая", ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            return;
        }
        model.removeAllRows();
     
        JOptionPane.showMessageDialog(this, "Таблица успешно очищена", INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_deleateAllRowsButtonActionPerfomed

    private void SaveToBinaryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveToBinaryButtonActionPerformed
        IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();
        if (model.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "Для сохранения необходимо заполнить таблицу", ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            return;
        }
        String file = System.nanoTime() + ".ser";
        try {
            Serializer.binarySerialization(model.getData(), file);
            JOptionPane.showMessageDialog(this, "Данные успешно записаны в файл " + file, INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
        } catch (SerializationException ex) {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getDescription(), ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_SaveToBinaryButtonActionPerformed

    private void UploadFromBinaryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UploadFromBinaryButtonActionPerformed
        JFileChooser fileopen = new JFileChooser();
        if (fileopen.showDialog(null, "Открыть файл") == JFileChooser.APPROVE_OPTION) {
            try {
                List<IntegrationUnit> fileRows = Serializer.binaryDeserialization(fileopen.getSelectedFile(), IntegrationUnit.class);
                IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();                
                model.addAllData(fileRows);
                JOptionPane.showMessageDialog(this, "Данные успешно добавлены", INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
            } catch (SerializationException ex) {
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getDescription(), ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_UploadFromBinaryButtonActionPerformed

    private void SaveToTextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveToTextButtonActionPerformed
        IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();
        if (model.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "Для сохранения необходимо заполнить таблицу", ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            return;
        }
        // создать папку ресурсы и туда пихать файл или еще круче куда пользователь захочет
        String file = System.nanoTime() + ".txt";
        try {
            Serializer.textSerialization(model.getData(), file);
            JOptionPane.showMessageDialog(this, "Данные успешно записаны в файл " + file, INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
        } catch (SerializationException ex) {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getDescription(), ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_SaveToTextButtonActionPerformed

    private void UploadFromTextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UploadFromTextButtonActionPerformed
        JFileChooser fileopen = new JFileChooser();
        if (fileopen.showDialog(null, "Открыть файл") == JFileChooser.APPROVE_OPTION) {
            try {
                List<IntegrationUnit> fileRows = Serializer.textDeserialization(fileopen.getSelectedFile(), IntegrationUnit.class);
                IntegrationUnitTableModel model = (IntegrationUnitTableModel) integrationTable.getModel();
                model.addAllData(fileRows);
                JOptionPane.showMessageDialog(this, "Данные успешно добавлены", INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
            } catch (SerializationException ex) {
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getDescription(), ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_UploadFromTextButtonActionPerformed
    
    private static final Logger logger = LoggerFactory.getLogger(MainUI.class);
    private static final String ERROR_TITLE = "Ошибка";
    private static final String INFO_TITLE = "Информация";
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton SaveToBinaryButton;
    private javax.swing.JButton SaveToTextButton;
    private javax.swing.JButton UploadFromBinaryButton;
    private javax.swing.JButton UploadFromTextButton;
    private javax.swing.JButton addButton;
    private javax.swing.JButton deleateAllRowsjButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel highLimitLabel;
    private javax.swing.JTextField highLimitTextField;
    private javax.swing.JTable integrationTable;
    private javax.swing.JLabel jLabelSimbolsAfterComma;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinnerSimbolsAfterComma;
    private javax.swing.JLabel lowLimitLabel;
    private javax.swing.JTextField lowLimitTextField;
    private javax.swing.JButton processButton;
    private javax.swing.JLabel stepLimitLabel;
    private javax.swing.JTextField stepLimitTextField;
    // End of variables declaration//GEN-END:variables
    
    /**
     * Creates new form MainUI
     */
    public MainUI() {
        initComponents();
        setTitle("UniSwing");
        setLocationRelativeTo(null);
        integrationTable.setModel(new IntegrationUnitTableModel());
        lowLimitTextField.setInputVerifier(new IntegrationTextInputVerifier());
        highLimitTextField.setInputVerifier(new IntegrationTextInputVerifier());
        stepLimitTextField.setInputVerifier(new IntegrationTextInputVerifier());
        jSpinnerSimbolsAfterComma.setModel(new SpinnerNumberModel(1, 1, 9, 1));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            logger.error(ex.getMessage());
        }

        EventQueue.invokeLater(() -> new MainUI().setVisible(true));
    }
    
}
